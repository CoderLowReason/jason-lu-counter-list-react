import './Counter.css'
import React from "react";

const Counter = (props) => {
    return (
        <div className="counter">
            <button className='button' onClick={props.decrease}>-</button>
            {props.count}
            <button className='button' onClick={props.increase}>+</button>
        </div>
    )
}

export default Counter