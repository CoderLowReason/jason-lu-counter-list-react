import {useDispatch} from "react-redux";
import {updateSize} from "../store/counterSlice";
import React from "react";

export function CounterSizeGenerator(props) {
    const dispatch = useDispatch();
    function handleSizeChange(e) {
      const number = parseInt(e.target.value);
      const size = isNaN(number) ? 0 : number;
      dispatch(updateSize(size));
    }
  
    return <div>
      <span>Size:</span>
      <input value={props.size || ''} onChange={handleSizeChange}/>
    </div>;
  }
  