import {render,screen } from "@testing-library/react";
import Counter from "./Counter";
import React from "react";

describe('Counter component test', () => {
    test('should render Counter component with count props', () => {
        const count = 10;
        render(<Counter count={count}/>)
        const countNode = screen.getByText(`${count}`);
        expect(countNode).toBeInTheDocument();
    })
})