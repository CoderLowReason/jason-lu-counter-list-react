import Counter from "./Counter";
import {useDispatch, useSelector} from "react-redux";
import {updateCounterList} from "../store/counterSlice";

export function CounterGroup() {
  const counterList = useSelector(state => state.counter.counterList);
  const dispatch = useDispatch();
  const handleCountChange = (index, diff) => () => {
    const newList = [...counterList]
    newList[index] += diff
    dispatch(updateCounterList(newList))
  }


  return <>
    <p>Here are {counterList.length} counters</p>
    {counterList.map((count, index) => (
      <Counter key={index} count={count} increase={handleCountChange(index,1)} decrease={handleCountChange(index, -1)}  />
    ))}
  </>;
}

