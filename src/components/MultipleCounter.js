import { CounterGroup } from "./CounterGroup";
import { CounterSizeGenerator } from "./CounterSizeGenerator";
import {CounterSum } from "./CounterSum";
import { useSelector} from "react-redux";
import React from "react";

export function MultipleCounter() {
  let counterList = useSelector(state => state.counter.counterList);
  const sum = counterList.reduce((prev, count) => prev + count, 0)

  return <>
    <CounterSizeGenerator size={counterList.length} />
    <CounterSum sum={sum} />
    <CounterGroup countList={counterList} />
  </>
}
