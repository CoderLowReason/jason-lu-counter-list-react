import './App.css';
import {MultipleCounter} from './components/MultipleCounter';
import React from "react";

function App() {
  return (
    <div className='container'>
      <MultipleCounter />
    </div>
  );
}

export default App;
