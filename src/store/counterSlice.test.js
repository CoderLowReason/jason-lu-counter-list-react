import counterSlice, {updateSize, updateCounterList} from "./counterSlice";

describe('countSlice reducers test', () => {
    it('should generate update size action when call updateSize', function () {
        let action = updateSize(2);
        expect(action).toEqual({"payload": 2, "type": "counter/updateSize"});
    });

    it('should get init state', function () {
        const initState = counterSlice({"counterList": [0]}, {});
        expect(initState).toEqual({
            counterList:[0],
        })
    });

    it('should change counter list with updateSize reducer', function () {
        const state = counterSlice({"counterList": [0]}, updateSize(3));
        expect(state).toEqual({
            counterList:[0,0,0],
        })
    });

    it('should update counterList with updateCounterList reducer', function () {
        const state = counterSlice({"counterList": [0]}, updateCounterList([1,2,3]));
        expect(state).toEqual({
            counterList:[1,2,3],
        })
    });
})