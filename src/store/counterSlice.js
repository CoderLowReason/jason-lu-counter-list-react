import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        counterList: []
    },
    reducers: {
        updateSize: (state, action) => {
            state.counterList = new Array(action.payload).fill(0);
        },
        updateCounterList: (state, action) => {
            state.counterList = [...action.payload];
        },
    }
})

export const { updateSize, updateCounterList } = counterSlice.actions

export default counterSlice.reducer